.data
msg: .asciiz "the largest number is: "

.text
.globl main
main:
    li $v0, 5           # read_int
    syscall
    move $t0, $v0
    li $v0, 5
    syscall
    move $t1, $v0

    addi $sp, $sp, -4   # save $ra
    sw $ra, 4($sp)      
    move $a0, $t0       # pass parameters
    move $a1, $t1
    jal Largest
    lw $ra, 4($sp)      # restore $ra
    addi $sp, $sp, 4
    jr $ra

Largest:
    move $t0, $a0       # move $a0 temporarily since syscall needs it
    li $v0, 4
    la $a0, msg
    syscall             # print message
    move $a0, $t0
    slt $t0, $a0, $a1
    blez $t0, output    # if $t0 <= 0 ($a0 >= $a1), goto output
    move $a0, $a1       # $a0 < $a1, we should output $a1
output:
    li $v0, 1
    syscall             # print largest number
    jr $ra


