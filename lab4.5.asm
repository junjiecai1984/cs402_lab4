.data
input_msg: .asciiz "input number: "
output_msg: .asciiz "factorial of input number is: "
err_msg: .asciiz "input is negative. try again.\n"

.text
.globl main
main:
    li $v0, 4           # display input message
    la $a0, input_msg
    syscall
    li $v0, 5           # read_int
    syscall
    move $t0, $v0
    bgez $t0, next      # if input >= 0, proceed
    li $v0, 4           # display error message
    la $a0, err_msg
    syscall
    j main
next:
    addi $sp, $sp, -4   # save $ra
    sw $ra, 4($sp) 
    move $a0, $t0       # pass parameters
    jal Factorial
    move $t0, $v0       # move return value because syscall also uses $v0
    li $v0, 4
    la $a0, output_msg
    syscall
    li $v0, 1           # print_int
    move $a0, $t0
    syscall
    lw $ra, 4($sp)      # restore $ra
    addi $sp, $sp, 4
    jr $ra

Factorial:
    addi $sp, $sp, -4   # save $ra
    sw $ra, 4($sp)
    beqz $a0, terminate
    addi $sp, $sp, -4   # save current parameter $a0
    sw $a0, 4($sp)
    sub $a0, $a0, 1
    jal Factorial       # call Factorial($a0 - 1)
    lw $t0, 4($sp)
    mul $v0, $v0, $t0   # $v0 <- $a0 * Factorial($a0 - 1)
    lw $ra, 8($sp)      # restore $ra
    addi $sp, $sp, 8    # pop saved $ra and $a0
    jr $ra

terminate:
    li $v0, 1
    lw $ra, 4($sp)
    addi $sp, $sp, 4
    jr $ra


